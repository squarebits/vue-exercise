Vue.component('calculator-button',{
template: '<button v-once v-on:click="actButton">{{value}}</button>',
props: ['value'],
  methods:{
    actButton: function(){
      this.$emit('action-clicked',this.value);
    }
  }
}
);

var viewModel = new Vue({
    el: '#app',
    data: {
      model: {
        currentOutputState: '',
        currentOperation: 'noop',
        firstParameter: '',
        secondParameter: '',
        filingSecondParameter: false,
        carryOver: false
      }
    },
    methods: {
      calculate: function(passValue){
        if(isNaN(passValue)){
          if(passValue === '='){
            var first = parseInt(this.model.firstParameter);
            var second = parseInt(this.model.secondParameter);

            var result = 0;
            switch(this.model.currentOperation){
              case '+': result = first + second; break;
              case '-': result = first - second; break;
              case '*': result = first * second; break;
              case '/': result = first / second; break;
            }

            this.model.currentOutputState = result;
            this.model.firstParameter = ''+result;
            this.model.secondParameter = '';
            this.model.filingSecondParameter = false;
            this.model.carryOver = true;
            this.model.currentOperation = 'noop';
          } else{
            this.model.filingSecondParameter = true;
            this.model.currentOperation = passValue;
          }
        } else{
          if(this.model.filingSecondParameter){
            this.model.secondParameter = this.model.secondParameter + passValue;
            this.model.currentOutputState = this.model.secondParameter;
          } else{
            if(this.model.carryOver){
              this.model.firstParameter = '' + passValue;
              this.model.carryOver = false;
            } else{
              this.model.firstParameter = this.model.firstParameter + passValue;
            }
            this.model.currentOutputState = this.model.firstParameter;
          }
        }
      }
    }
  });